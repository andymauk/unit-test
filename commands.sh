#!/bin/bash
mkdir -p ~/projects/stam/L03/unit-test
code ~/projects/stam/L03/unit-test
git init
#alusta npm
npm init -y
#asenna express
npm i --save express
#asenna mocha chai
npm i --save-dev mocha chai
#create gitignore
echo "node_modules" > .gitignore
#add remote repository address
git add remote origin https:gitlab.com/andymauk/unit-test/
#katso mihin menossa
git remote -v
#katso tiedostot mitä menossa
git add . --dry-run

touch README.md
mkdir src test
touch src/main.js src/calc.js
touch public/index.html
mkdir public
touch public/index.html
touch test/calc.test.js

#package.json type add
"type":"module",

#käynnistys komento talteen package.jsoniin
"scripts": {
    "start": "node src/main.js",
    "dev": "node --watch-path=src src/main.js",}

